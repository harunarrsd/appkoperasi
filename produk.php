<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Koperasi SMK N 1 DEPOK</title>
<link rel="shortcut icon" href="1.png" type="image/x-icon">
<script type="text/javascript">
function MM_preloadImages() { //v3.0
  var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
    var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
}
function MM_swapImgRestore() { //v3.0
  var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
}
function MM_findObj(n, d) { //v4.01
  var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
    d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
  if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
  for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
  if(!x && d.getElementById) x=d.getElementById(n); return x;
}

function MM_swapImage() { //v3.0
  var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
   if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
}
</script>
<style type="text/css">
a:link,a:visited {
text-decoration:none;
color:#FFFFFF;
}

a:hover,a:active {
color:#000;
text-decoration:none;
}
body {
	background-color: #008CFF;
	margin-top: -8px;
}
.table2 {
	background-color: #FFFFFF;
	color: #FFFFFF;
}
.table2 tr th #logo {
	margin-bottom: -2pt;
}
body,td,th {
	font-size: medium;
	font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
}
.table2 tr th .tableproduk {
	text-align: center;
	background-color: #008CFF;
	color: #FFFFFF;
	border-radius: 20pt;
}
.table2 tr th .tableproduk tr #kosong {
	background-color: #FFFFFF;
}
.table3 {
	margin-top: 14pt;
	background-color: #0113DF;
	font-size: small;
	color: #FFFFFF;
	font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
}
.table4 {
	background-color: #FFFFFF;
}
.table4 tr td table tr td #belian {
	margin-left: 7pt;
	margin-top: -9pt;
	color: #FFFFFF;
	background-color: #008CFF;
}
</style>
</head>
<body onLoad="MM_preloadImages('1.png','ATK/2.jpg')">
<table width="80%" height="171" border="0" align="center" cellpadding="0" cellspacing="0" class="table2">
    <tr>
      <th width="5%" scope="col" bgcolor="#0113DF">
	  <p>S</p>
      <p>M</p>
      <p>K</p>
      <p>N</p></th>
      <th width="33%" scope="col"><a href="index.php"><input name="logo" type="image" id="logo" src="1.png" width="250" height="150"></a></th>
      <th width="57%" height="171" scope="col"><table width="211" height="46" border="0" align="center" cellpadding="0" cellspacing="0" class="tableproduk">
        <tr>
          <td width="100" height="46" id="produk"><a href="produk.php">PRODUK</a></td>
          <td width="11" id="kosong">&nbsp;</td>
          <td width="100" id="login"><a href="ADMIN/index.php">LOGIN</a></td>
        </tr>
      </table></th>
      <th width="5%" scope="col" bgcolor="#0113DF">
	  <p>1</p>
      <p>D</p>
      <p>P</p>
      <p>K</p></th>
    </tr>
</table>
<form method="post" action="beli.php">
<table width="80%" height="63" border="0" align="center" cellpadding="0" cellspacing="0" class="table3">
    <tr>
      <td><marquee direction="left" behavior="alternate" scrollamount="5">Selamat datang di Aplikasi Koperasi SMK N 1 Depok. Temukan berbagai barang dan produk, menarik dan terpopuler! Enjoying look in.</marquee></td>
  </tr>
</table>
<table width="80%" border="0" align="center" cellpadding="0" cellspacing="0" class="table4">
  <tr>
    <td width="25%" style="color: #000000"><p><img src="ATK/1.jpg" alt="" width="265" height="150" id="Image2"></p></td>
    <td width="23%" style="color: #000000"><p>Joyko Ball Pen BP_179 Sprit</p>
    <p>Rp 28.000/pcs </p>
    <table width="147" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><input type="submit" name="belian" id="belian" value="Beli"></td>
      </tr>
    </table></td>
    <td width="25%" style="color: #000000"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image3','','ATK/2.jpg',1)"><img src="ATK/2.jpg" alt="" width="250" height="150" id="Image3"></a></td>
    <td width="27%" style="color: #000000"><p>Zebra Pulpen Paket Sarasa Clip 07</p>
    <p>Rp. 30.000/pcs</p>
    <table width="96" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><input name="belian" type="submit" id="belian" value="Beli"></td>
      </tr>
    </table></td>
  </tr>
</table>
</form>
</body>
</html>