-- phpMyAdmin SQL Dump
-- version 4.5.0.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: 29 Nov 2015 pada 09.33
-- Versi Server: 5.6.21
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `koperasidb`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `datapembelian`
--

CREATE TABLE `datapembelian` (
  `id` int(50) NOT NULL,
  `hari_tanggal` text NOT NULL,
  `nama` text NOT NULL,
  `nisn` varchar(20) NOT NULL,
  `nama_barang` varchar(50) NOT NULL,
  `harga` varchar(50) NOT NULL,
  `kelas` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `datapembelian`
--

INSERT INTO `datapembelian` (`id`, `hari_tanggal`, `nama`, `nisn`, `nama_barang`, `harga`, `kelas`) VALUES
(9, '2015-11-28', 'HARUN', '9995111832', 'joyko ballpen', '2500', 'XI RPL 2');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tableanggota`
--

CREATE TABLE `tableanggota` (
  `No` int(50) NOT NULL,
  `hari_tanggal` varchar(50) NOT NULL,
  `namaanggota` varchar(50) NOT NULL,
  `kelas` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tableanggota`
--

INSERT INTO `tableanggota` (`No`, `hari_tanggal`, `namaanggota`, `kelas`, `password`) VALUES
(22, '2015-11-29', 'HARUN', 'XI RPL 2', '123456');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tablebarang`
--

CREATE TABLE `tablebarang` (
  `No` int(250) NOT NULL,
  `JenisBarang` text NOT NULL,
  `NamaBarang` text NOT NULL,
  `Persediaan` varchar(250) NOT NULL,
  `Harga` varchar(250) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `tablebarang`
--

INSERT INTO `tablebarang` (`No`, `JenisBarang`, `NamaBarang`, `Persediaan`, `Harga`) VALUES
(28, 'Alat Tulis Kantor', 'Pensil 2B', '2 Pcs', '20000');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `user_id` int(5) NOT NULL,
  `username` varchar(25) DEFAULT NULL,
  `email` varchar(35) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`user_id`, `username`, `email`, `password`) VALUES
(4, 'Harun', 'Harun@gmail.com', '827ccb0eea8a706c4c34a16891f84e7b'),
(5, 'Danty', 'danty@gmail.com', 'e10adc3949ba59abbe56e057f20f883e'),
(6, 'harun', 'harunarrosid07@gmail.com', '22892764c73ee2244e0e24becac707b7');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `datapembelian`
--
ALTER TABLE `datapembelian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tableanggota`
--
ALTER TABLE `tableanggota`
  ADD PRIMARY KEY (`No`);

--
-- Indexes for table `tablebarang`
--
ALTER TABLE `tablebarang`
  ADD PRIMARY KEY (`No`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `datapembelian`
--
ALTER TABLE `datapembelian`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tableanggota`
--
ALTER TABLE `tableanggota`
  MODIFY `No` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `tablebarang`
--
ALTER TABLE `tablebarang`
  MODIFY `No` int(250) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
