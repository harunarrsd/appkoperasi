<?php
session_start();

if(!isset($_SESSION['user']))
{
 header("Location: anggota.php");
}
else if(isset($_SESSION['user'])!="")
{
 header("Location: homeanggota.php");
}

if(isset($_GET['logout']))
{
 session_destroy();
 unset($_SESSION['user']);
 header("Location: anggota.php");
}
?>