<!doctype html>
<html><head>
<meta charset="utf-8">
<title>Koperasi SMK N 1 DEPOK</title>
<link rel="shortcut icon" href="1.png" type="image/x-icon">
<style type="text/css">
a:link,a:visited {
text-decoration:none;
color:#FFFFFF;
}
a:hover,a:active {
color:#000;
text-decoration:none;
}
body {
	background-color: #008CFF;
	margin-top: -8px;
}
.table2 {
	background-color: #FFFFFF;
	color: #FFFFFF;
}
.table3 {
	margin-top: 14pt;
	background-color: #0113DF;
	font-size: small;
	color: #FFFFFF;
	font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
}
.table4 {
	background-color: #FFFFFF;
	font-size: large;
	font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
	text-align: justify;
}
body,td,th {
	font-size: medium;
	font-family: Segoe, "Segoe UI", "DejaVu Sans", "Trebuchet MS", Verdana, sans-serif;
}
.table4 tr td a #Image1 {
	margin-bottom: 111pt;
}
#Image1 {
	margin-left: 98pt;
	margin-top: -23pt;
}
.table5 {
	background-color: #FFFFFF;
	margin-top: -6pt;
}
.table2 tr th #logo {
	margin-bottom: -2pt;
}
.table2 tr th .tableproduk {
	text-align: center;
	background-color: #008CFF;
	color: #FFFFFF;
	border-radius: 20pt;
}
.table2 tr th .tableproduk tr #kosong {
	background-color: #FFFFFF;
}
</style>
</head>

<body onLoad="MM_preloadImages('1.png')">
<table width="80%" height="171" border="0" align="center" cellpadding="0" cellspacing="0" class="table2">
    <tr>
      <th width="5%" scope="col" bgcolor="#0113DF">
	  <p>S</p>
      <p>M</p>
      <p>K</p>
      <p>N</p></th>
      <th width="33%" scope="col"><a href="index.php"><input name="logo" type="image" id="logo" src="1.png" width="250" height="150"></a></th>
      <th width="57%" height="171" scope="col"><table width="211" height="46" border="0" align="center" cellpadding="0" cellspacing="0" class="tableproduk">
        <tr>
          <td width="100" height="46" id="produk"><a href="produk.php">PRODUK</a></td>
          <td width="11" id="kosong">&nbsp;</td>
           <td width="100" id="login"><a href="ADMIN/index.php">LOGIN</a></td>
       </tr>
      </table></th>
      <th width="5%" scope="col" bgcolor="#0113DF">
	  <p>1</p>
      <p>D</p>
      <p>P</p>
      <p>K</p></th>
    </tr>
</table>
<table width="80%" height="63" border="0" align="center" cellpadding="0" cellspacing="0" class="table3">
    <tr>
      <td><marquee direction="left" behavior="alternate" scrollamount="5">Selamat datang di Aplikasi Koperasi SMK N 1 Depok. Temukan berbagai barang dan produk, menarik dan terpopuler! Enjoying look in.</marquee></td>
  </tr>
</table>
<table width="80%" height="200" border="0" align="center" cellpadding="0" cellspacing="0" class="table4">
  <tr>
    <td width="28%" height="196"><a href="#" onMouseOut="MM_swapImgRestore()" onMouseOver="MM_swapImage('Image2','','1.png',1)"><img src="1.png" alt="" width="292" height="192" id="Image2"></a></td>
    <td width="72%"><p>Koperasi sekolah atau koperasi siswa adalah koperasi yang anggota-anggotanya terdiri atas siswa-siswa sekolah dasar, sekolah menengah pertama, sekolah menengah atas, atau sekolah-sekolah yang sederajat. Koperasi merupakan organisasi yang telah banyak dikenal oleh hampir seluruh lapisan masyarakat, namun pada kenyataannya masih banyak lapisan masyarakat yang belum memahami sepenuhnya seluk beluk perkoperasian.</p>
      <ul>
        <li>Landasan Koperasi Sekolah</li>
      </ul>
    <p>Seperti koperasi pada umumnya, koperasi sekolah memiliki landasan hukum yang kuat, yang meliputi</p></td>
  </tr>
</table>
<table width="80%" border="0" align="center" cellpadding="0" cellspacing="0" class="table5">
  <tr>
    <td><p>landasan ideal, konstitusional, dan landasan operasional. Landasan ideal dan konstitusional koperasi sekolah adalah Pancasila dan Undang-Undang Dasar 1945. Adapun landasan operasional koperasi sekolah diatur dalam keputusan bersama Menteri Tenaga Kerja, Transmigrasi dan Koperasi serta Menteri Pendidikan dan Kebudayaan No. 638/SKPTS/Men/1994, mengenai pembinaan dan pengembangan koperasi sekolah.</p>
      <ul>
        <li>Ciri-Ciri Koperasi Sekolah</li>
      </ul>
      <p>Ciri-ciri koperasi sekolah, di antaranya sebagai berikut: </p>
      <ol>
        <li>Koperasi sekolah didirikan dalam rangka kegiatan belajar mengajar para siswa di sekolah.</li>
        <li>Anggotanya adalah kalangan siswa/murid sekolah yang bersangkutan.</li>
        <li>Bentuk koperasi sekolah tidak berbadan hukum karena pendiriannya berkaitan dengan kegiatan belajar mengajar.</li>
        <li>Berfungsi sebagai laboratorium atau media praktik untuk pengajaran koperasi sekolah.</li>
      </ol>
      <ul>
        <li>Fungsi dan Tujuan Koperasi Sekolah</li>
      </ul>
      <p>Koperasi sekolah berfungsi sebagai wadah untuk mendidik bagi tumbuhnya kesadaran  berkoperasi di kalangan siswa. Adapun tujuan koperasi sekolah adalah sebagai berikut.</p>
      <ol>
        <li> Mendidik, menanamkan, dan memelihara suatu kesadaran hidup bergotong-royong, serta jiwa demokratis di antara para siswa. </li>
        <li>Memupuk dan mendorong tumbuhnya kesadaran serta semangat berkoperasi di kalangan siswa.</li>
        <li>Mendidik dan menanamkan jiwa kewirausahaan (entrepreneurship) di kalangan siswa.</li>
        <li>Meningkatkan pengetahuan dan keterampilan berkoperasi di kalangan anggota yang  berguna bagi para siswa untuk bekal terjun di masyarakat.</li>
        <li>Menunjang program pembangunan pemerintah di sektor perkoperasian melalui  program pendidikan sekolah.</li>
        <li>Membantu dan melayani pemenuhan kebutuhan ekonomi para siswa melalui  pengembangan koperasi sekolah.</li>
    </ol></td>
  </tr>
</table>
</body>
</html>